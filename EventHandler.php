<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class EventHandler
{
    static private $instance;
    private $events = [];

    /**
     * Magic Call Static
     * -----------------
     * The single point of entry into the Event Handler class, used to create a
     * fallback method for static method calls.
     * It also serves the purpose of loading an instance of itself into the static
     * instance property and then makes a call on that property to create object context
     */
    public static function __callStatic($name, $arguments)
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance->callStaticMethod($name, $arguments);
    }

    /**
     * Call Static Method
     * ------------------
     * Use the data from the fallback static method call and call a function within
     * the new instance of this object.
     */
    public function callStaticMethod($name, $arguments)
    {
        return call_user_func_array([get_class($this),$name],$arguments);
    }

    /**
     * Register Event
     * ---------
     * Register an event handle against a function call passing through an optional
     * key for which data needs to be passed into the function's parameters.
     *
     * @param   String  $key    The key of the function being registered.
     * @param   Object  $classObject  The class of the function being called.
     * @param   String  $functionMethod The method of the function being called.
     * @param   Array   $parameters An array of data indexes being passed to the
     * parameters.
     */
    private function registerEvent($key, $classObject, $functionMethod, Array $parameters = [], $priority = 999)
    {
        $this->events[$key][$priority][] = [
        'class' => $classObject,
        'method' => $functionMethod,
        'parameters' => $parameters
        ];
        $this->events = array_map(function($key){
            ksort($key);
            return $key;
        },$this->events);
    }

    /**
     * Trigger Event
     * -------------
     * Trigger a previously registered event.
     *
     * @param   String  $key    The key of the function being called.
     * @param   Array   $data   An array containing the data used for the parameters.
     */
    private function triggerEvent($key, Array $data = [])
    {
        $priorities = isset($this->events[$key]) ? $this->events[$key] : null;
        if ($priorities) {
            foreach ($priorities as $priority => $functions) {
                foreach ($functions as $function) {
                    // Check all required keys have been provided
                    $missingData = array_diff($function['parameters'], array_keys($data));
                    if (!empty($missingData)) {
                        throw new Exception('Event: ' . $key . ' failed; Missing parameters: ' . substr(implode(', ', $missingData), 0, -1));
                        return false;
                    }
                    // Call function with parameters
                    call_user_func_array(
                       array($function['class'], $function['method']),
                       array_combine($function['parameters'],$data)
                    );
                }
            }
        }
    }

    /**
     * List Events
     * -----------
     * List the currently registered events (or single event if key is provided),
     * along with their function calls and parameter requirements.
     *
     * @param   String  $key    An optional key for a single function.
     * @return  Array   An array containing all the information regarding the
     * requested registered events.
     */
    private function listEvents($key = null)
    {
        if ($key !== null && isset($this->events[$key])) {
            return $this->events[$key];
        }
        return $this->events;
    }
}
